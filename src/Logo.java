import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class Logo implements Runnable {
	
	static JFrame frame = new JFrame("Logo Compiler");
	static int sizing = 80;
	static JTextArea instructions;
	static JTextArea progtitle, error;
	static Turtle turtle;
	static Program p = new Program();
	
	static boolean showError = true;
	static JPanel errorpanel;
	static int animationspeed = 150;
	static boolean animate = true;
	static double zoom = 1; // double as in 1x zoom
	static int animationSpeed = 200;
	static int winSize = 900;
	static Program program;
	
	public static void main(String[] args) throws IOException {
		loadLibrary();
		SwingUtilities.invokeLater(new Logo()); 
	}
	
	public void run() {
		frame.setBounds(100*sizing/100, 100*sizing/100, 1550*sizing/100, 830*sizing/100);	
		frame.setVisible(true);
		// emergency stop
		   try 
		    { 
		        UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"); 
		    }    catch(Exception e){ 
		    }
		frame.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
        addMenu();

	    turtle = new Turtle();

	      JPanel buttonpanel = new JPanel();    
	      frame.add(buttonpanel,   BorderLayout.NORTH);   
	      buttonpanel.setBorder(BorderFactory.createLineBorder(Color.black));
	      
	      buttonpanel.setPreferredSize(new Dimension(900*sizing/100, 50*sizing/100));                 // clean button
    		JButton cleanbutton = new JButton("Clean");
    		buttonpanel.add(cleanbutton);
    		cleanbutton.addActionListener(new ActionListener() {
    			public void actionPerformed(ActionEvent evt) {
    				turtle.clean();
    				turtle.repaint();
    			}
    		});

    		// reset button
	      		JButton resetbutton = new JButton("Reset & Clean");
	      		buttonpanel.add(resetbutton);
	      		resetbutton.addActionListener(new ActionListener() {
	      			public void actionPerformed(ActionEvent evt) {
	      				turtle.reset();
	      				turtle.repaint();
	      			}
	      		});                                                     // reset button

	      		// run button
	      		JButton runbutton = new JButton("Load & Run");
	      		buttonpanel.add(runbutton);
	      		runbutton.addActionListener(new ActionListener() {
	      			public void actionPerformed(ActionEvent evt) {
	      				loadInstructions(instructions.getText(),progtitle.getText());
	      				executeInstructions(turtle);               // gets instructions and runs them
	      				turtle.repaint();
	      			}
	      		});
	      		               // run button
		      		JButton quickbutton = new JButton("Quick Draw");
		      		buttonpanel.add(quickbutton);
		      		quickbutton.addActionListener(new ActionListener() {
		      			public void actionPerformed(ActionEvent evt) {
		      				animate = false;
		      				loadInstructions(instructions.getText(),progtitle.getText());
		      				executeInstructions(turtle);               // gets instructions and runs them
		      				turtle.repaint();
		      				animate = true;
		      			}
		      		});


	      frame.add(turtle);
	      

	      JPanel instructionpanel = new JPanel();    
	      frame.add(instructionpanel,   BorderLayout.WEST); 
	      instructionpanel.setBorder(BorderFactory.createLineBorder(Color.black));
	      instructionpanel.setPreferredSize(new Dimension(400*sizing/100, 750*sizing/100));
	      progtitle = new JTextArea(1, 16);
	      instructions = new JTextArea(25, 16);
	      instructions.setFont(new Font("Verdana", Font.BOLD, 20*sizing/100));
	      instructionpanel.add(progtitle);
	      progtitle.setFont(new Font("Verdana", Font.BOLD, 20*sizing/100));
	      instructionpanel.add(new JScrollPane(instructions));
	      
	      errorpanel = new JPanel();    
	      frame.add(errorpanel,   BorderLayout.EAST); 
	      errorpanel.setBorder(BorderFactory.createLineBorder(Color.black));
	      errorpanel.setPreferredSize(new Dimension(400*sizing/100, 750*sizing/100));
	      error = new JTextArea(25, 16);
	      error.setFont(new Font("Verdana", Font.BOLD, 20*sizing/100));
	      error.setForeground(Color.RED);
	      error.setLineWrap(true);
	      error.setWrapStyleWord(true);
	      errorpanel.add(new JScrollPane(error));
	      error.setEditable(false);
	      

	}
	
	public static void loadInstructions() {
		loadInstructions(instructions.getText(),progtitle.getText());
	}
	
	public static void loadInstructions(String s, String t) {
		p = new Program(t);
		String[] lines = s.split("\\r?\\n");
		for (String c : lines) {
			String[] splited = (c+" 0").split(" ");
			p.commands.add(new Command(splited[0],Integer.parseInt(splited[1])));
		}
		errorCheck();
	}
	
	public static void executeInstructions(Turtle t) {
		executeInstructions(p, t);
	}
	public static void executeInstructions(Program curr, Turtle t) {
		for (Command c : expandProgram(curr).commands) {
			if (c.action.equals("fw") || c.action.equals("forward")) {
				t.move(c.value*zoom);
			} else if (c.action.equals("bk") || c.action.equals("back")) {
				t.turn(180);
				t.move(c.value*zoom);
				t.turn(180);
			}
			else if (c.action.equals("rt") || c.action.equals("right"))
				t.turn(c.value);
			else if (c.action.equals("lf") || c.action.equals("left"))
				t.turn(0-c.value);
			else if (c.action.equals("pu") || c.action.equals("penup"))
				t.penDown = false;
			else if (c.action.equals("pd") || c.action.equals("pendown"))
				t.penDown = true;
			else if (c.action.equals("blue"))
				t.color = Color.BLUE;
			else if (c.action.equals("red"))
				t.color = Color.red;
			else if (c.action.equals("green"))
				t.color = Color.green;
			else if (c.action.equals("white"))
				t.color = Color.white;
			else if (c.action.equals("black"))
				t.color = Color.black;
			else if (c.action.equals("clean"))
				t.clean();
			else if (c.action.equals("pause"))
				pause((long) c.value);
			else {
				boolean member = false;
				Program subprogram = null;
				for (Program p : library) {
					if (p.name.equals(c.action)) {
						member = true;
						subprogram = p;
					}
				}
				if (member) { executeInstructions(subprogram, t);}
				else return;                              //error message for unknown instruction
			}
 			if (animate) {
				t.paintImmediately(0,0,700,700);
				pause();
			}
		}
	}
	
	
	public static Program expandProgram(Program p) {
		Program newp = new Program(p.name);
		for (int i = 0; i < p.commands.size(); i++) {
			int repeatIndex;
			if (p.commands.get(i).action.equals("repeat")) {
				repeatIndex = i;
				int repeatcount = 1;
				while (repeatcount != 0)
				       { repeatIndex++;
					     if (p.commands.get(repeatIndex).action.equals("repeat"))
				    	   repeatcount++;
				         if (p.commands.get(repeatIndex).action.equals("end"))
					       repeatcount--;} // calculates the endrepeat index
				for (int n = (int) p.commands.get(i).value; n > 0; n--) {
					Program subp = new Program();
					for (int x = i+1; x < repeatIndex; x++) {
						subp.commands.add(p.commands.get(x));
					}
					subp = expandProgram(subp);
					for (Command c : subp.commands) {
						newp.add(c);
					}
					
				}
				i = repeatIndex;
			}
			else {newp.add(p.commands.get(i)); }
		}
		return newp;
		
	}
	
	  public static void pause() {
		    	    pause(animationSpeed);
	  }
	
	  public static void pause(long delay) {
		     try {
		    	    Thread.sleep(delay);
		       }
		     catch (InterruptedException e) {
		       e.printStackTrace();
		       }
	  }  
	  
	  
	  
	  
	  
	  //  --------------------------------------- the jmenu ------------------
	  public static void addMenu() {
		   JMenuBar menuBar = new JMenuBar();
		   frame.setJMenuBar(menuBar);
		   
		   JMenu file = new JMenu("File");
		   menuBar.add(file);
		   JMenuItem save = new JMenuItem("Save");
		   file.add(save);
    		save.addActionListener(new ActionListener() {
      			public void actionPerformed(ActionEvent evt) {
      				  saveProgram();
      			}
      		});		   
    		save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
		   JMenuItem saveas = new JMenuItem("Save As");
		   file.add(saveas);
    		saveas.addActionListener(new ActionListener() {
      			public void actionPerformed(ActionEvent evt) {
      				  saveAsProgram();
      			}
      		});
		   JMenuItem newP = new JMenuItem("New");
		   file.add(newP);
    		newP.addActionListener(new ActionListener() {
      			public void actionPerformed(ActionEvent evt) {
      				  newProgram();
      			}
      		});
    		newP.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK));
 		   JMenuItem open = new JMenuItem("Open");
 		   file.add(open);
     		open.addActionListener(new ActionListener() {
       			public void actionPerformed(ActionEvent evt) {
       				  openProgram();
       			}
       		});
     		open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK));
     		
    		
  		   
		   
    		
    		
		   JMenu view = new JMenu("View");
		   menuBar.add(view);
		   
		   JMenuItem zoomr = new JMenuItem("Zoom 100%");
		   view.add(zoomr);
     		zoomr.addActionListener(new ActionListener() {
      			public void actionPerformed(ActionEvent evt) {
      				  zoom = 1;
      				  turtle.reset();
      				  executeInstructions(turtle);
      			}
      		});

     		zoomr.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0, KeyEvent.SHIFT_DOWN_MASK));
		   JMenuItem zoomi = new JMenuItem("Zoom In");
		   view.add(zoomi);
     		zoomi.addActionListener(new ActionListener() {
      			public void actionPerformed(ActionEvent evt) {
      				  zoom = zoom *1.6;
      				  animate = false;
      				  turtle.reset();
      				  executeInstructions(turtle);
      				  turtle.repaint();
      				  animate = true;
      			}
      		});
     		zoomi.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, KeyEvent.SHIFT_DOWN_MASK));
 		   JMenuItem zoomo = new JMenuItem("Zoom Out");
 		   view.add(zoomo);
      		zoomo.addActionListener(new ActionListener() {
       			public void actionPerformed(ActionEvent evt) {
       				  zoom = zoom /1.6;
       				  animate = false;
      				  turtle.reset();
      				  executeInstructions(turtle);
      				  turtle.repaint();
      				  animate = true;
       			}
       		});
            zoomo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,KeyEvent.SHIFT_DOWN_MASK));
            
            
            
            JMenu library = new JMenu("Library");
 		   menuBar.add(library);
 		   JMenuItem imprt = new JMenuItem("Import from Disk");
 		   library.add(imprt);
     		imprt.addActionListener(new ActionListener() {
       			public void actionPerformed(ActionEvent evt) {
       				  try {
						loadFromHardDrive();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
       			}
       		});		   
     		imprt.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, KeyEvent.CTRL_DOWN_MASK)); 
  		   JMenuItem exprt = new JMenuItem("Export to disk");
  		   library.add(exprt);
      		exprt.addActionListener(new ActionListener() {
        			public void actionPerformed(ActionEvent evt) {
        				try {
							loadFromHardDrive();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
        				  saveToHardDrive();
        			}
        		});		     
     		exprt.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_DOWN_MASK));  
     		library.addSeparator();
    		   JMenuItem manage = new JMenuItem("Manage Library");
    		   library.add(manage);
        		manage.addActionListener(new ActionListener() {
          			public void actionPerformed(ActionEvent evt) {
          				delete();
          			}
          		});		     
     		library.addSeparator();
   		   JMenuItem wipe = new JMenuItem("Wipe Library");
   		   library.add(wipe);
       		wipe.addActionListener(new ActionListener() {
         			public void actionPerformed(ActionEvent evt) {
         				clearLibrary();
         			}
         		});		     
	  
	  
	  
	  
      JMenu settings = new JMenu("Settings");
	   menuBar.add(settings);
	   JMenuItem sett = new JMenuItem("Change Settings");
	   settings.add(sett);
		sett.addActionListener(new ActionListener() {
 			public void actionPerformed(ActionEvent evt) {
 				  settings();
 			}
 		});	
}
	  
	  
	  //   --------------------------------- library functions -------------------------------------
	  
	  static ArrayList<Program>  library;
	  public static void loadLibrary() throws IOException {
		  library = new ArrayList<Program>();
		  loadFromHardDrive();
	  }
	  
	  public static void clearLibrary() {
		  JFrame warning = new JFrame("WARNING! WARNING! WARNING!");
		  warning.setVisible(true);
		  warning.setBounds(150*sizing/100,150*sizing/100,700*sizing/100,200*sizing/100);
		  JLabel text = new JLabel("WARNING: Are you sure you want to delete the");
		  text.setFont(new Font("Verdana", Font.BOLD, 20*sizing/100));
		  JLabel text2 = new JLabel("entire library and wipe the hard drive?");
		  text2.setFont(new Font("Verdana", Font.BOLD, 20*sizing/100));
		  JPanel textlab = new JPanel();
		  textlab.add(text, BorderLayout.NORTH);
		  warning.add(textlab, BorderLayout.NORTH);
		  JPanel textlab2 = new JPanel();
		  textlab2.add(text2, BorderLayout.NORTH);
		  warning.add(textlab2);
		  
		  JPanel warningbuttons = new JPanel();
		  warning.add(warningbuttons, BorderLayout.SOUTH);
		  JButton delete = new JButton("Delete");
		  warningbuttons.add(delete);
		  delete.addActionListener(new ActionListener() {
     			public void actionPerformed(ActionEvent evt) {
     				  library = new ArrayList<Program>();
       	      		warning.setVisible(false);
     			}
     		});
		  JButton cancel = new JButton("Cancel");
		  warningbuttons.add(cancel);
		  cancel.addActionListener(new ActionListener() {
     			public void actionPerformed(ActionEvent evt) {
     	      		warning.setVisible(false);
     			}
     		});
	  }
	  
	  public static void newProgram() {
		  JFrame warning = new JFrame("Warning");
			  warning.setVisible(true);
		  warning.setBounds(400*sizing/100,400*sizing/100,530*sizing/100,150*sizing/100);
		  JLabel label = new JLabel("Warning: changes to program will be lost");
		  label.setFont(new Font("Verdana", Font.BOLD, 20*sizing/100));
		  warning.add(label, BorderLayout.NORTH);
		  JPanel butts = new JPanel();
		  warning.add(butts, BorderLayout.SOUTH);
		  JButton saveb = new JButton("Save");
	      butts.add(saveb);
    		saveb.addActionListener(new ActionListener() {
       			public void actionPerformed(ActionEvent evt) {
       				  warning.setVisible(false);
       				  saveProgram();
       	      		instructions.setText("");
       	      		progtitle.setText("");
       	      		warning.setVisible(false);
       			}
       		});
    		JButton discardb = new JButton("Discard");
  	      butts.add(discardb, BorderLayout.SOUTH);
      		discardb.addActionListener(new ActionListener() {
         			public void actionPerformed(ActionEvent evt) {
           	      		instructions.setText("");
           	      		progtitle.setText("");
         				  warning.setVisible(false);
         			}
         		});
	  }
	  
	  public static void saveProgram() {
		  loadInstructions();
		  for (Program prog : library) {
			  if (prog.name.equals(p.name)) {
				  prog.commands = p.commands;
  				  saveToHardDrive();
				  return;
			  }
		  }
		  saveAsProgram();
	  }
	  
	  public static void saveAsProgram() {
		  loadInstructions();
		  JFrame saveFrame = new JFrame("Save As");
		  ArrayList<String> foldNames = new ArrayList<String>();
		  for (Program pro : library) {
			  boolean add = true;
			  for (String n : foldNames) {
				  if (n.equals(pro.folder)) add = false;
			  }
			  if (add) foldNames.add(pro.folder);
		  }
		  saveFrame.setBounds(300*sizing/100,300*sizing/100,900*sizing/100,500*sizing/100);
		  saveFrame.setVisible(true);
		  JTextArea dir = new JTextArea(10, 13);
		  saveFrame.add(new JScrollPane(dir), BorderLayout.WEST);
		  dir.append("Existing Folders:\n");
		  dir.setFont(new Font("Verdana", Font.BOLD, 15*sizing/100));
		  for (String f : foldNames) {
			  dir.append("  - "+f+"\n");
		  }
		  
		  JPanel pan = new JPanel();
		  saveFrame.add(pan,    BorderLayout.NORTH);
		  JLabel pn = new JLabel("            Program: ");
		  pn.setFont(new Font("Verdana", Font.BOLD, 25*sizing/100));
		  pan.add(pn);
	      JTextArea programName = new JTextArea(1, 16);
	      programName.setFont(new Font("Verdana", Font.BOLD, 20*sizing/100));
	      programName.setText(p.name);
	      pan.add(programName,   BorderLayout.NORTH);
	      
		  JPanel pan2 = new JPanel();
		  JLabel pn2 = new JLabel("Folder: ");
		  pn2.setFont(new Font("Verdana", Font.BOLD, 25*sizing/100));
		  pan2.add(pn2);
		  saveFrame.add(pan2);
	      JTextArea programFolder = new JTextArea(1, 16);
	      programFolder.setFont(new Font("Verdana", Font.BOLD, 20*sizing/100));
	      programFolder.setText(p.folder);
	      pan2.add(programFolder,   BorderLayout.SOUTH);
	      
	      JButton savebutt = new JButton("Save");
	      pan2.add(savebutt);
    		savebutt.addActionListener(new ActionListener() {
       			public void actionPerformed(ActionEvent evt) {
       				  p.folder = programFolder.getText();
       				  p.name = programName.getText();
       				  for (Program prog : library) {
       					  if (prog.name.equals(p.name)) {
       						  prog.folder = programFolder.getText();
       						  prog.commands = p.commands;
       	      				  saveToHardDrive();
       	      				  saveFrame.setVisible(false);
       						  return;
       					  }
       				  }
       				  library.add(p);
      				  saveToHardDrive();
      				  progtitle.setText(programName.getText());
       				  saveFrame.setVisible(false);
       			}
       		});
	  }
	  
	  
	  public static void openProgram() {
			  sortLibrary();
		  JFrame openFrame = new JFrame("Open");
		  openFrame.setBounds(300,300,500,500);
		  openFrame.setVisible(true);
		  

		   // get all directories
		  ArrayList<String> foldNames = new ArrayList<String>();
		  for (Program pro : library) {
			  boolean add = true;
			  for (String n : foldNames) {
				  if (n.equals(pro.folder)) add = false;
			  }
			  if (add) foldNames.add(pro.folder);
		  }
		  

		  JTextArea dir = new JTextArea(18, 25);
		  openFrame.add(new JScrollPane(dir), BorderLayout.NORTH);
	      dir.setEditable(false);
		  dir.append("Root Dir:\n");
		  dir.setFont(new Font("Verdana", Font.BOLD, 15));
		  for (String f : foldNames) {
			  dir.append("  |- "+f+"\n");
			  for (Program prog : library) {
				  if (prog.folder.equals(f)) 
					  dir.append("        |-- "+prog.name+"\n");
			  }
		  }

		  JPanel selpan = new JPanel();
		  JLabel sellabel = new JLabel("Open:");
		  sellabel.setFont(new Font("Verdana", Font.BOLD, 18*sizing/100));
		  selpan.add(sellabel);
		  JTextArea selection = new JTextArea(1, 15);
		  selection.setFont(new Font("Verdana", Font.BOLD, 18*sizing/100));
		  selpan.add(selection);
		  JButton openbutt = new JButton("Open");
		  selpan.add(openbutt);
		  openFrame.add(selpan, BorderLayout.SOUTH);
    		openbutt.addActionListener(new ActionListener() {
       			public void actionPerformed(ActionEvent evt) {
       				boolean exists = false;
       				Program foundProgram = new Program();
       				for (Program pr : library)
       					{ if (pr.name.equals(selection.getText())) {
       						foundProgram = pr;
       						exists = true;
       						}
       					}
       				if (exists) {
       					progtitle.setText(foundProgram.name);
       					instructions.setText("");
       					for (Command c : foundProgram.commands) {
       						instructions.append(c.action);
       						if (((int)c.value) != 0) 
           						instructions.append(" "+((int)c.value)+"\n");
       						else instructions.append("\n");
       					}
       					p = foundProgram;
       					openFrame.setVisible(false);
       				} else {
       					JFrame alert = new JFrame();
       					JLabel a = new JLabel("Error: not a saved program");
       				  a.setFont(new Font("Verdana", Font.BOLD, 18*sizing/100));
       					alert.add(a);
       					alert.setBounds(500*sizing/100,500*sizing/100,400*sizing/100,100*sizing/100);
       					alert.setVisible(true);
       				}
       			}
       		});
	  }
	  
	  public static void saveToHardDrive() {
		  saveToHardDrive("library.txt");
	  }
	  
	  public static void saveToHardDrive(String filename) {
		  PrintWriter writer;
		try {
			writer = new PrintWriter(filename, "UTF-8");
			  writer.println(library.size());
              for (Program program : library) {
            	  writer.println(program.name);
            	  writer.println(program.folder);
            	  for (Command c : program.commands) {
            		  writer.println(c.action+" "+((int)c.value));
            	  }
            	  writer.println("###");
              }
			  writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
	  
	  public static void loadFromHardDrive() throws IOException {
		  FileReader fr = new FileReader("library.txt");
		  BufferedReader in = new BufferedReader(fr);
		  int numOfPrograms =Integer.parseInt(in.readLine());
		  for (int i = 0; i < numOfPrograms; i++) {
			  Program newprogram = new Program();
			  String pname = in.readLine();
			  newprogram.name = pname;
			  String pfolder = in.readLine();
			  newprogram.folder = pfolder;
			  String nextLine = in.readLine();
			  while (!(nextLine.equals("###"))) {
				  System.out.println(nextLine);
					String[] splited = (nextLine+" 0").split(" ");
					Command c = new Command(splited[0],Integer.parseInt(splited[1]));
					newprogram.add(c);
				  nextLine = in.readLine();
			  }
			  library.add(newprogram);
		  }
	  }
	  
	  public static void settings() {
		  JFrame settingFrame = new JFrame("Settings");
		  settingFrame.setBounds(200*sizing/100,200*sizing/100,600*sizing/100,270*sizing/100);
		  settingFrame.setVisible(true);
		  
		  JPanel anipanel = new JPanel();
		  settingFrame.add(anipanel, BorderLayout.NORTH);
		  JLabel anilab = new JLabel("Animation Speed: ");
		  anilab.setFont(new Font("Verdana", Font.BOLD, 22*sizing/100));
		  anipanel.add(anilab);
		  JTextArea anispeed = new JTextArea(1, 5);
		  anispeed.setFont(new Font("Verdana", Font.BOLD, 18*sizing/100));
		  anipanel.add(anispeed);
		  anispeed.setText(animationSpeed+"");
		  JLabel anilab2 = new JLabel("Delay in milliseconds");
		  anipanel.add(anilab2);
		  

		  JPanel eppanel = new JPanel();
		  settingFrame.add(eppanel);
		  JLabel eplab = new JLabel("Show Error Panel: ");
		  eplab.setFont(new Font("Verdana", Font.BOLD, 22));
		  eppanel.add(eplab);
		    JRadioButton showbutton = new JRadioButton("Show");
			  showbutton.setFont(new Font("Verdana", Font.BOLD, 22));
		    JRadioButton hidebutton = new JRadioButton("Hide");
			  hidebutton.setFont(new Font("Verdana", Font.BOLD, 22));
		    ButtonGroup group = new ButtonGroup();
		    group.add(showbutton);
		    group.add(hidebutton);
		    eppanel.add(showbutton);
		    eppanel.add(hidebutton);
		    if (showError) showbutton.setSelected(true);
		    else hidebutton.setSelected(true);


			  JPanel showTpanel = new JPanel();
			  eppanel.add(showTpanel);
			  JLabel showTlab = new JLabel("Show Turtle: ");
			  showTlab.setFont(new Font("Verdana", Font.BOLD, 22*sizing/100));
			  showTpanel.add(showTlab);
			    JRadioButton showTbutton = new JRadioButton("Show Turtle");
				  showTbutton.setFont(new Font("Verdana", Font.BOLD, 22*sizing/100));
			    JRadioButton hideTbutton = new JRadioButton("Hide Turtle");
				  hideTbutton.setFont(new Font("Verdana", Font.BOLD, 22*sizing/100));
			    ButtonGroup groupT = new ButtonGroup();
			    groupT.add(showTbutton);
			    groupT.add(hideTbutton);
			    showTpanel.add(showTbutton);
			    showTpanel.add(hideTbutton);
			    if (turtle.showTurtle) showTbutton.setSelected(true);
			    else hideTbutton.setSelected(true);
		    
		    JPanel buttonpanel = new JPanel();
			  settingFrame.add(buttonpanel, BorderLayout.SOUTH);
			  JButton save = new JButton("Save");
			  buttonpanel.add(save);
	      		save.addActionListener(new ActionListener() {
	      			public void actionPerformed(ActionEvent evt) {
	      				animationSpeed = Integer.parseInt(anispeed.getText());
	      				showError = showbutton.isSelected();
	      				errorpanel.setVisible(showError);
	      				turtle.showTurtle = showTbutton.isSelected();
	      				turtle.repaint();
	      				settingFrame.setVisible(false);
	      			}
	      		});  
	  } 
	  
	  // ------------------------------- error checking ------------------------------

	  public static void errorCheck() {
		  error.setText("");
		  // check loops "repeats and ends"
		  int loopcounter = 0;
		  for (Command c : p.commands) {
			  if (c.action.equals("repeat")) loopcounter++;
			  if (c.action.equals("end")) loopcounter--;
		  }
		  if (loopcounter > 0) error.append("\n\nError: repeat not terminated");
		  if (loopcounter < 0) error.append("\n\nError: extra expression \"end\"");
		  
		  // check for invalid commands
		  String allMethods = "fw forward rt right back bk lf left";
		  allMethods += " pu penup pd pendown repeat end pause clean";
		  allMethods += " blue black red green white";
		  for (Program p : library) { allMethods += " " + p.name;  }
		  String[] methods = (allMethods).split(" ");
		  for (Command c : p.commands) {
			  boolean valid = false;
			  for (String name : methods) {
				  if (name.equals(c.action)) valid = true;
			  }
			  if (!valid) error.append("\n\nError: unknown command \""+c.action+"\"");
		  }
	  }
	  
	  
	  //  ------------------------------------------ sorting Library algorithms ----------------
	  public static boolean isLess(String a, String b) {
		  int blength = b.length();
		  for (int i = 0; i < a.length(); i++) {
			  b += " ";  
		  }
		  for (int i = 0; i < blength; i++) {
			  a += " ";
		  }
		  for (int i = 0; i < b.length(); i++) {
			  if (a.charAt(i) > b.charAt(i)) return false;
		  }
		  return true;
	  }
	  
	  public static Program extractSmallestProgram() {
		  Program smallest = library.get(0);
		  for (Program prog : library) {
			  if (! isLess(smallest.name,prog.name)) smallest = prog;
		  }
		  return smallest;
	  }
	  
	  public static void sortLibrary() {
		  ArrayList<Program> newLib = new ArrayList<Program>();
		  while (library.size() > 0) {
			  Program smallest = extractSmallestProgram();
			  library.remove(smallest);
			  newLib.add(smallest);
		  }
		  library = newLib;
	  }
	  
	  public static void delete() {
		  sortLibrary();
	  JFrame deleteFrame = new JFrame("Delete");
	  deleteFrame.setBounds(300*sizing/100,300*sizing/100,600*sizing/100,500*sizing/100);
	  deleteFrame.setVisible(true);
	  

	   // get all directories
	  ArrayList<String> foldNames = new ArrayList<String>();
	  for (Program pro : library) {
		  boolean add = true;
		  for (String n : foldNames) {
			  if (n.equals(pro.folder)) add = false;
		  }
		  if (add) foldNames.add(pro.folder);
	  }
	  

	  JTextArea dir = new JTextArea(18, 25);
	  deleteFrame.add(new JScrollPane(dir), BorderLayout.NORTH);
      dir.setEditable(false);
	  dir.append("Root Dir:\n");
	  dir.setFont(new Font("Verdana", Font.BOLD, 15*sizing/100));
	  for (String f : foldNames) {
		  dir.append("  |- "+f+"\n");
		  for (Program prog : library) {
			  if (prog.folder.equals(f)) 
				  dir.append("        |-- "+prog.name+"\n");
		  }
	  }

	  JPanel selpan = new JPanel();
	  JLabel sellabel = new JLabel("Delete Program:");
	  sellabel.setFont(new Font("Verdana", Font.BOLD, 18*sizing/100));
	  selpan.add(sellabel);
	  JTextArea selection = new JTextArea(1, 15);
	  selection.setFont(new Font("Verdana", Font.BOLD, 18*sizing/100));
	  selpan.add(selection);
	  JButton deletebutt = new JButton("Delete");
	  selpan.add(deletebutt);
	  deleteFrame.add(selpan);
		deletebutt.addActionListener(new ActionListener() {
   			public void actionPerformed(ActionEvent evt) {
   				boolean exists = false;
   				Program foundProgram = new Program();
   				for (Program pr : library)
   					{ if (pr.name.equals(selection.getText())) {
   						foundProgram = pr;
   						exists = true;
   						}
   					}
   				if (exists) {
   	   				deleteProgram(selection.getText());
   	   				saveToHardDrive();
   	   				deleteFrame.setVisible(false);
   				} else {
   					JFrame alert = new JFrame();
   					JLabel a = new JLabel("Error: not a saved program");
   					a.setFont(new Font("Verdana", Font.BOLD, 18*sizing/100));
   					alert.add(a);
   					alert.setBounds(500*sizing/100,500*sizing/100,400*sizing/100,100*sizing/100);
   					alert.setVisible(true);
   				}
   			}
   		});
		  JPanel selpan2 = new JPanel();
		  JLabel sel2label = new JLabel("Delete Folder:");
		  sel2label.setFont(new Font("Verdana", Font.BOLD, 18*sizing/100));
		  selpan2.add(sel2label);
		  JTextArea selection2 = new JTextArea(1, 15);
		  selection2.setFont(new Font("Verdana", Font.BOLD, 18*sizing/100));
		  selpan2.add(selection2);
		  JButton delete2butt = new JButton("Delete");
		  selpan2.add(delete2butt);
		  deleteFrame.add(selpan2, BorderLayout.SOUTH);
			deletebutt.addActionListener(new ActionListener() {
	   			public void actionPerformed(ActionEvent evt) {
	   				boolean exists = false;
	   				Program foundProgram = new Program();
	   				for (Program pr : library)
	   					{ if (pr.name.equals(selection.getText())) {
	   						foundProgram = pr;
	   						exists = true;
	   						}
	   					}
	   				if (exists) {
	   					deleteFolder(selection.getText());
	   					saveToHardDrive();
	   					deleteFrame.setVisible(false);
	   				} else {
	   					JFrame alert = new JFrame();
	   					JLabel a = new JLabel("Error: not a saved folder");
	   					a.setFont(new Font("Verdana", Font.BOLD, 18*sizing/100));
	   					alert.add(a);
	   					alert.setBounds(500*sizing/100,500*sizing/100,400*sizing/100,100*sizing/100);
	   					alert.setVisible(true);
	   				}
	   			}
	   		});
		  
		  
	  }
	  
	  public static void deleteFolder(String n) {
		  for (int i = 0; i < library.size(); i++) {
			  if (library.get(i).folder.equals(n)) {
				  library.remove(i);
				  i--;
			  }
		  }
	  }	  
	  
	  public static void deleteProgram(String n) {
		  for (int i = 0; i < library.size(); i++) {
			  if (library.get(i).name.equals(n)) {
				  library.remove(i);
				  i--;
			  }
		  }
	  }
}