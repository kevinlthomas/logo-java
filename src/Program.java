import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JFrame;

public class Program {
	
	ArrayList<Command> commands;
	String name;
	String folder = "";
	
	public Program() {
		name = "untitled";
		commands = new ArrayList<Command>();
		
	}
	
	public Program(String s) {
		name = s;
		commands = new ArrayList<Command>();
	}
	

	
	public void add(Command c) {
		commands.add(c);
	}
	

}