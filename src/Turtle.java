import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JComponent;

@SuppressWarnings("serial")
public class Turtle extends JComponent  {
	
	public double radius = 350;
	
	public static double x;
	public static double y;
	public static double angle; // angle is positive clockwise starting with 0 at due north or up
	public boolean penDown;
	public Color color;
	public static ArrayList<Line> lines = new ArrayList<Line>();
	public boolean showTurtle;

	public class Line {
		
		public double x1,y1,x2,y2;
		public Color color;
		public Line(double x, double y, double xb, double yb, Color c) {
			x1 = x;
			y1 = y;
			x2 = xb;
			y2 = yb;
			color = c;
		}
	}
	
	public Turtle() {
		x = radius;
		y = radius;
		angle = 0;
		penDown = true;
		color = Color.black;
		showTurtle = true;
	}
	
	@Override
	public void paintComponent(Graphics gc) {
		//blank background
		gc.setColor(Color.white);
		gc.fillRect(0, 0, (int)radius*2, (int)radius*2);
		
		for (Line l : lines) {
			gc.setColor(l.color);
			gc.drawLine((int)l.x1, (int)l.y1, (int)l.x2, (int)l.y2);
		}
		

		if (showTurtle) {
			//OG turtle
			gc.setColor(Color.gray);
			gc.drawLine((int)radius,(int)radius,(int)radius+10,(int)radius+25);
			gc.drawLine((int)radius+10,(int)radius+25,(int)radius-10,(int)radius+25);
			gc.drawLine((int)radius-10,(int)radius+25,(int)radius,(int)radius);
				
			// current turtle				
			gc.setColor(Color.red);
			double xb = x + 22 * Math.sin(Math.toRadians(angle-22.80140949));
			double xc = x + 22 * Math.sin(Math.toRadians(angle+22.80140949));
			int[] xs = {(int)x,(int)xb,(int)xc};
			double yb = y + 22 * Math.cos(Math.toRadians(angle-22.80140949));
			double yc = y + 22 * Math.cos(Math.toRadians(angle+22.80140949));
			int[] ys = {(int)y,(int)yb,(int)yc};
			gc.fillPolygon(xs,ys,3);
		}
		


	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(100,100);
	}
	
	public void turn(double d) {
		angle -= d;
		angle = angle % 360;
		repaint();
	}
	
	public void move(double s) {
		double xo = x;
		double yo = y;
		x -= s * Math.sin(Math.toRadians(angle));
		y -= s * Math.cos(Math.toRadians(angle));
		if (penDown) {
			lines.add(new Line(xo,yo,x,y,color));
		}
		repaint();
	}
	
	public void penUp() {
		penDown = false; 
	}
	
	public void penDown() {
		penDown = true;
	}
	
	public String toString() {
		return ("("+x+","+y+") @ "+angle+"* E of N");
	}
	
	public void reset() {
		x = radius;
		y = radius;
		angle = 0;
		penDown = true;
		color = Color.black;
		lines = new ArrayList<Line>();
		repaint();
	}
	
	public void clean() {
		lines = new ArrayList<Line>();
		repaint();
	}
	

}